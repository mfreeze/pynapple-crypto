#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Pynapple IRC Client. Copyright 2013 Windsor Schmidt <windsor.schmidt@gmail.com>
#
# Notes/caveats:
#
# > doesn't handle large nick-lists (i.e. lists spanning multiple 353 messages)
#
# By the way, this line is 80 characters long....................................

import argparse

from interface.keyboard_handler import KeyboardHandler
from interface.user_interface import UserInterface
from irc.irc import IRC


if __name__ == "__main__":
    # Argument parsing
    parser = argparse.ArgumentParser(description="Set parameters for irc client")

    parser.add_argument("-a", "--attacker-key", 
                        help="Specify attacker cypher key", default=None)
    parser.add_argument("-d", "--defender-key", 
                        help="Specify defender cypher key", default=None)
    parser.add_argument("-c", "--channel", 
                        help="Specify a channel to connect to", 
                        default="#pynapple")
    parser.add_argument("-l", "--log", help="Specify logfile",
                        default="log.txt")
    parser.add_argument("-L", "--message-log-file", 
                        help="Specify a file that will contain all the encrypted messages", 
                        default="cyphered.log")
    parser.add_argument("-n", "--nick", help="Specify IRC nickname",
                        default="pynapple")
    parser.add_argument("-N", "--name", help="Specify user real name",
                        default="Pynapple User")
    parser.add_argument("-p", "--port", 
                        help="Specify port on which server is listening", 
                        type=int, default=6667)
    parser.add_argument("-r", "--role", help="Specify attacker or defender", 
                        default="attacker",
                        choices=["attacker", "defender"])
    parser.add_argument("-s", "--server", 
                        help="Specify a server to connect to", 
                        default="irc.libera.chat")
    parser.add_argument("-u", "--user", help="Specify user name",
                        default="pynapple")

    args = parser.parse_args()

    if args.channel and args.channel[0] != '#':
        args.channel = '#' + args.channel

    irc = IRC(attacker_key=args.attacker_key,
            channel=args.channel,
            defender_key=args.defender_key,
            log_file=args.log,
            message_log_file=args.message_log_file,
            name=args.name, 
            nick=args.nick, 
            port=args.port,
            role=args.role,
            server=args.server,
            user=args.user)

    kb = KeyboardHandler(irc=irc)
    ui = UserInterface(irc_instance=irc, keyboard_handler=kb)

    kb.register_ui(ui)
    irc.ui = ui
    ui.run()
