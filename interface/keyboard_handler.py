import sys

class KeyboardHandler:
    def __init__(self, 
                 lastInput="",
                 irc=None,
                 ui=None):
        self.lastInput=lastInput
        self.ui = ui
        self.irc = irc

    def print_status(self, message):
        self.ui.add_status_message(message)
        print(message, file=sys.stderr)

    def register_ui(self, ui):
        self.ui = ui

    def parse_input(self, s):
        # Parse local user input and handle by dispatching commands or sending
        # messages to the current IRC channel.
        if ((s == "/") and (self.lastInput != "")):
            # Protip: Re-use the last input if a single forward slash is entered.
            s = self.lastInput
        self.lastInput = s
        if (s[0] == '/'):
            if (len(s) > 1):
                # got a command; handle locally,
                self.handle_cmd(s[1:])
        else:
            # otherwise send input as a channel message
            self.irc.send_message(s)

    def handle_cmd(self, s):
        # Respond to a command string intended to be processed locally.
        cmd = s.split()[0]
        args = s.split()[1:]
        if (cmd == "connect"):
            # Connect to the given IRC server.
            if (len(args) == 1) and (args[0].count(":") == 1):
                server, port = args[0].split(':')
                if not len(str(port)):
                    port="6667"
                if port.isdigit():
                    self.print_status("connecting to " + server + ":" + port)
                    self.irc.connect(server, int(port))
                else:
                    self.print_status("port must be specified as an integer")
            else:
                self.print_status("usage: connect <server:port>")
        elif (cmd == "setakey"):
            if (len(args) < 1):
                self.print_status("usage: setakey <attacker_key>")
            else:
                dkey = "".join(args)
                if len(dkey) < 26:
                    dkey += "a" * (26 - len(dkey))
                elif len(dkey) > 26:
                    dkey = dkey[:26]
                self.irc.update_attacker_key(dkey)
        elif (cmd == "reinitakey"):
            self.irc.reinit_attacker_key()
        elif (cmd == "setdkey"):
            if (len(args) < 1):
                self.print_status("usage: setdkey <defender_key>")
            else:
                dkey = "".join(args)
                self.irc.update_defender_key(dkey)
        elif (cmd == "reinitdkey"):
            self.irc.reinit_defender_key()
        elif (cmd == "disconnect"):
            # Disconnect from the current IRC server.
            self.irc.part()
            self.irc.disconnect()
        elif (cmd == "join"):
            # Join the given channel.
            if (len(args) < 1):
                self.print_status("usage: join <channel>")
            else:
                self.irc.join(args[0])
        elif (cmd == "part"):
            # Leave the current channel.
            self.irc.part()
        elif (cmd == "msg"):
            # Send a private message to the given user.
            if (len(args) < 2):
                self.print_status("usage: msg <nick> <message>")
            else:
                msg = ' '.join(args[1:])
                self.irc.send_private_message(args[0], msg)
        elif (cmd == "nick"):
            if (len(args) < 1):
                self.print_status("usage: nick <new nick>")
            else:
                self.irc.set_nick(args[0])
        elif (cmd == "debug"):
            # Show or hide the debug window.
            self.ui.toggle_debug()
        elif (cmd == "names"):
            # Ask server for a list of nicks in the channel. TODO: Remove this.
            self.irc.request_nicklist()
        elif (cmd == "help"):
            # Print a list of commands.
            self.print_status("available commands:")
            self.print_status("/connect <server:port>")
            self.print_status("/disconnect")
            self.print_status("/join <channel>")
            self.print_status("/msg <nick> <message>")
            self.print_status("/nick <new nick>")
            self.print_status("/part")
            self.print_status("/quit")
            self.print_status("/reinitakey")
            self.print_status("/reinitdkey")
            self.print_status("/setakey <attacker_key>")
            self.print_status("/setdkey <defender_key>")
        elif (cmd == "quit"):
            # Quit the program.
            self.irc.part()
            self.irc.disconnect()
            self.ui.shutdown()
            exit()
        elif (cmd == "test"):
            self.irc.connect("localhost", 6667)
            self.irc.join("#pynapple")
        else:
            # The user entered an unknown command, punish them!
            msg = "unknown command: " + cmd
            self.print_status(msg)
        self.lastCommandString = s

