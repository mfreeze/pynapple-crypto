from irc.irc import IRC
from interface.pynapple_tkui import UserInterfacePlugin
from datetime import datetime
import hashlib

class UserInterface:
    # Uses the curses terminal handling library to display a chat log,
    # a list of users in the current channel, and a command prompt for
    # entering messages and application commands.
    badwords = []
    hilites = []
    def __init__(self, irc_instance, keyboard_handler):
        self.irc = irc_instance
        self.badwords = self.load_list("badwords.txt")
        self.hilites = self.load_list("hilites.txt")
        self.uiPlugin = UserInterfacePlugin(irc_instance,
                                            keyboard_handler)
        self.colors = self.uiPlugin.get_max_colors()
        self.draw_pineapple()
        self.add_status_message("welcome to pynapple-irc v" + self.irc.get_version())
        self.add_status_message("type /help for a list of commands")

    def run(self):
        self.uiPlugin.run()

    def add_message(self, s, color, hilite):
        msgtxt = self.censor(s)
        msg = self.time_stamp() + " " + msgtxt
        self.uiPlugin.add_message(msg, color, hilite)

    def add_nick_message(self, nick, s):
        # Add another user's message in the chat window.
        color = self.get_nick_color(nick)
        hilite = False
        if (nick != self.irc.get_nick()):
            hilite = self.hilite(s)
        self.add_message("<" + nick + "> " + s, color, hilite)

    def add_emote_message(self, nick, s):
        # Add another user's "emoted" message in the chat window.
        color = self.get_nick_color(nick)
        if (nick != self.irc.get_nick()):
            hilite = self.hilite(s)
        self.add_message("* " + nick + " " + s, color, hilite)

    def add_private_message(self, nick, s):
        # Add another user's private message in the chat window.
        self.add_nick_message(nick, "[private] " + s)

    def add_status_message(self, s):
        # Add a status message in the chat window.
        self.add_message("== " + s, 7, False)

    def add_debug_message(self, s):
        self.uiPlugin.add_debug_message(s)

    def hilite(self, s):
        # Return an true if the given word matches our highlight list.
        # The attribute is combined with any other attributes (e.g. colors)
        # when printing string. It is typical for IRC clients to highlight
        # incoming messages containing our own nick.
        if any(w in s for w in self.hilites + [self.irc.get_nick()]):
            return True
        else:
            return False

    def set_nicklist(self, a):
        # Populate the nick-list with an alphabetically sorted array of nicks.
        self.uiPlugin.set_nicklist(a)

    def init_colors(self):
        self.uiPlugin.init_colors()

    def get_nick_color(self, s):
        # It is often helpful to color messages based on the nick of the message
        # sender. Map an input string (the nick) to a color ID using Python's
        # hashing functions. The modulo operator here is used to map the hash
        # output value to a range within bounds of the color look up table.
        return(int(hashlib.md5(s.encode('utf-8')).hexdigest(), 16) % self.colors)

    def shutdown(self):
        self.uiPlugin.shutdown()

    def toggle_debug(self):
        self.uiPlugin.toggle_debug()

    def draw_pineapple(self):
        # Draw a sweet ASCII art rendition of a pinapple. Come to think of it,
        # it has been getting increasingly difficult to type the word pinapple
        # without replacing the "i" with a "y" instead.
        self.add_message("                \\\\//", 2, False)
        self.add_message("                \\\\//", 2, False)
        self.add_message("                \\\\//", 2, False)
        self.add_message("                /..\\", 3, False)
        self.add_message("                |..|", 3, False)
        self.add_message("                \\__/", 3, False)

    def time_stamp(self):
        # Generate a string containing the current time, used to prefix messages.
        return datetime.now().strftime("[%H:%M]")

    def load_list(self, s):
        # A utility function that loads each line from a given file in to a list.
        try:
            with open(s) as f:
                lines = f.readlines()
                f.close()
        except IOError:
            return []
        return [x.strip() for x in lines]

    def censor(self, s):
        # Replace bad words with an equal length string of asterisks
        for tag in self.badwords:
            s = s.replace(tag, "*" * len(tag))
        return s

    def update_status(self):
        self.uiPlugin.update_status()

