import socket
import threading

class SocketThread(threading.Thread):
    # A worker thread used to receive data from the connected IRC server. Once
    # started, sits in a loop reading data and assembling line-based messages
    # from the server. This thread terminates after a shared status flag is set
    # by the main thread in response to a disconnect command.
    running = True
    def __init__(self, event, rxQueue, server, port, sock):
        super(SocketThread, self).__init__()
        self.stopThreadRequest = event
        self.rxQueue = rxQueue
        self.server = server
        self.port = port
        self.sock = sock

    def run(self):
        # Continuously read from our (blocking) socket. We want to add complete
        # messages from the IRC server to our queue to be handled downstream, but
        # since the network buffer may contain only part of a message, we'll use
        # a local buffer to store incomplete messages.
        rx = ""
        while(not self.stopThreadRequest.isSet()):
            rx = rx + self.sock.recv(1024).decode("utf-8")
            if (rx != ""):
                temp = rx.split("\n")
                rx = temp.pop( ) # put left-over data back in our local buffer
                for line in temp:
                    line = line.rstrip()
                    self.rxQueue.put(line)
            else:
                # remote end disconnected, so commit thread suicide!
                self.stopThreadRequest.set()
        return

