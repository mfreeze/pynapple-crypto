import queue
from datetime import datetime
import hashlib
import threading
import string
import time
import socket

from utils.socket_thread import SocketThread
from crypto.mono_alphabetic_substitution import MonoAlphabeticSubstitution
from crypto.vigenere import Vigenere

from interface.pynapple_tkui import *
#from pynapple_ncui import *

class IRC:
    # Encapsulates a connection to an IRC server. Handles sending / receiving of
    # messages, message parsing, connection and disconnection, etc.
    host="localhost"
    partMessage = "Parting!"
    quitMessage = "Quitting!"
    version = "0.0000001"
    nicklist = []
    connected = False
    joined = False
    logEnabled = False
    stopThreadRequest = threading.Event()
    rxQueue = queue.Queue()

    def __init__(self, 
                 attacker_key=None,
                 channel=None,
                 defender_key=None,
                 log_file="log.txt",
                 message_log_file="cyphered.txt",
                 name="Pynapple",
                 nick="pynapple", 
                 port=6667,
                 role="attacker",
                 server=None,
                 topic="",
                 user="pynapple",
                 ui=None
                 ):
        """Constructor

        :nick: TODO
        :channel: TODO
        :server: TODO
        :topic: TODO
        :logfile: TODO
        :returns: TODO

        """
        self.nick = nick
        self.channel = channel
        self.server = server
        self.port = port
        self.topic = topic
        self.user = user
        self.name = name
        self.log_file = log_file
        self.message_log_file = message_log_file
        self.role = role
        self.ui = ui
        if attacker_key is not None:
            self.monosub = MonoAlphabeticSubstitution(attacker_key)
        else:
            self.monosub = MonoAlphabeticSubstitution("abcdefghijklmnopqrstuvwxyz")
        self.initial_monosub = self.monosub

        if defender_key is not None:
            self.vigenere = Vigenere(defender_key)
        else:
            self.vigenere = Vigenere("z")
        self.initial_vigenere = self.vigenere

        if self.server is not None:
            self.connect(self.server, self.port)
            if self.channel is not None:
                self.join(self.channel)

        if message_log_file is not None:
            self.message_fd = open(message_log_file, "a")
            self.cyphered_logging = True
        else:
            self.cyphered_logging = False

    def update_attacker_key(self, new_key):
        self.monosub = MonoAlphabeticSubstitution(new_key)

    def reinit_attacker_key(self):
        self.monosub = self.initial_monosub

    def update_defender_key(self, new_key):
        self.vigenere = Vigenere(new_key)

    def reinit_defender_key(self):
        self.vigenere = self.initial_vigenere
        

    def start_thread(self):
        # Spawn a new thread to handle incoming data. This function expects that
        # the class variable named socket is a handle to a currently open socket.
        self.socketThread = SocketThread(self.stopThreadRequest,
                                         self.rxQueue,
                                         self.server,
                                         self.port,
                                         self.sock)
        self.stopThreadRequest.clear()
        self.socketThread.start()

    def stop_thread(self):
        # Signal the socket thread to terminate by setting a shared event flag.
        self.stopThreadRequest.set()

    def print_status(self, message):
        try:
            self.ui.add_status_message(message)
        except:
            print(message)

    def print_debug(self, message):
        try:
            self.ui.add_debug_message(message)
        except:
            print("DEBUG: " + message)
        

    def connect(self, server, port):
        # Connect to an IRC server using a given host name and port. Creates a
        # network socket that is used by a separate thread when receiving data.
        if (not self.connected):
            self.server = server
            self.port = port
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect((self.server, self.port))
            self.start_thread()
            self.print_status("connecting to %s:%s" % (server, str(port)))
            self.connected = True
            self.login(self.nick, self.user, self.name, self.host, server)
        else:
            self.print_status("already connected")

    def send(self, command):
        # Send data to a connected IRC server.
        if (self.connected):
            self.sock.send(bytes(command + '\n', 'UTF-8'))
            self.print_debug("-> " + command)

    def send_message(self, s):
        # Send a message to the currently joined channel.
        if (self.joined):
            if self.role == "attacker":
                self.ui.add_nick_message("Private", s)
                cyphered = self.monosub.cypher(s)
            elif self.role == "defender":
                self.ui.add_nick_message("Private", s)
                cyphered = self.vigenere.cypher(s)
            else:
                cyphered = s
            m = "%s <-> %s" % (self.role, cyphered)
            self.ui.add_nick_message(self.get_nick(), m)
            self.send("PRIVMSG %s :%s" % (self.channel, m))
        else:
            self.print_status("not in a channel")

    def send_private_message(self, nick, s):
        # Send a private message to the given nickname.
        if (self.connected):
            self.send("PRIVMSG %s :%s" % (nick, s))
            self.ui.add_nick_message(self.get_nick(), "[%s] %s" % (nick, s))
        else:
            self.print_status("not connected")

    def get_status(self):
        return (self.nick, self.server, self.channel, self.topic)

    def disconnect(self):
        # Disconnect from the currently connected IRC server.
        if (self.connected):
            self.send("QUIT :%s" % self.quitMessage)
            self.stop_thread()
            self.connected = False
            self.server = ""
            self.print_status("disconnected")
            self.ui.update_status()
            if self.cyphered_logging:
                self.message_fd.close()
        else:
            self.print_status("not connected")

    def login(self, nick, user, name, host, server):
        # Send a log-in stanza to the currently connected server.
        self.send("USER %s %s %s %s" % (user, host, server, name))
        self.send("NICK %s" % nick)
        self.print_status("using nickname %s" % nick)

    def join(self, channel):
        # Join the given channel.
        if (self.connected):
            if (not self.joined):
                self.send("JOIN %s" % channel)
            else:
                self.print_status("already in a channel")
        else:
            self.print_status("not connected")

    def part(self):
        # Leave the current channel.
        if (self.joined):
            self.send("PART %s" % self.channel)
            self.set_nicklist([])
            self.print_status("left channel %s " % self.channel)
            self.joined = False
            self.channel = ""
            self.ui.update_status()
        else:
            self.print_status("not in a channel")

    def add_nick(self, s):
        # Add a nickname to the list of nicknames we think are in the channel.
        # Called when a user joins the current channel, in response to a join.
        self.nicklist.append(s)
        self.nicklist.sort()
        self.ui.set_nicklist(self.nicklist)

    def del_nick(self, s):
        # Remove a nickname the list of nicknames we think are in the channel.
        if s in self.nicklist:
            self.nicklist.remove(s)
            self.ui.set_nicklist(self.nicklist)

    def replace_nick(self, old, new):
        self.del_nick(old)
        self.add_nick(new)
        self.ui.set_nicklist(self.nicklist)
        self.print_status("%s is now known as %s" % (old, new))

    def request_nicklist(self):
        # Send a request to the IRC server to give us a list of nicknames
        # visible in the current channel.
        if (self.joined):
            self.send("NAMES %s" % self.channel)

    def set_nicklist(self, a):
        # Replace the list of nicknames with the list given.
        self.nicklist = a
        self.ui.set_nicklist(self.nicklist)

    def set_nick(self, s):
        # Change our own nickname.
        if (self.connected):
            self.send(":%s!%s@%s NICK %s" % (self.nick, self.user, self.host, s))

    def get_nick(self):
        # Return our own nickname.
        return self.nick

    def get_channel(self):
        # Return the name of the currently joined channel.
        if (self.joined):
            return self.channel
        else:
            return "~"

    def is_connected(self):
        # Return our IRC server connection state.
        return self.connected

    def handle_ctcp(self, cmd, msg):
        self.print_status("got CTCP message: " + cmd)
        if (cmd == "VERSION"):
            self.send("VERSION pynapple-irc %s" % self.version)
        if (cmd == "ACTION"):
            self.ui.add_emote_message(self.nick, msg)

    def get_version(self):
        return self.version

    def logToFile(self, s):
        # Write the given string to a log file on disk, appending a newline.
        # The logfile is opened for writing if not already open.
        if (not self.logEnabled):
            self.logEnabled = True
            self.file = open(self.log_file, 'w')
        self.file.write(s + "\n")
        self.file.flush()

    def poll(self):
        # Check for incoming messages from the IRC server by polling a shared
        # message-queue populated by the socket handling thread. Strings read
        # from the queue have been buffered from the receiving socket and each
        # string represents a logical message sent by the server.
        rx = ""
        try:
            rx = self.rxQueue.get(True, 0.01)
        except:
            pass
        if (rx != ""):
            self.print_debug("<- " + rx)
            self.logToFile(rx)
            self.handle_message(self.parse_message(rx))

    def parse_message(self, s):
        # Transform incoming message strings received by the IRC server in to
        # component parts common to all messages.
        prefix = ''
        trailing = []
        if (s[0] == ':'):
            prefix, s = s[1:].split(' ', 1)
        if (s.find(' :')) != -1:
            s, trailing = s.split(' :', 1)
            args = s.split()
            args.append(trailing)
        else:
            args = s.split()
        command = args.pop(0)
        return prefix, command, args

    def handle_message(self, msg):
        # Respond to incoming IRC messages by handling them here or passing
        # control to other class methods for further processing.
        prefix, cmd, args = msg
        if (cmd == "PING"):
            # Reply to PING, per RFC 1459 otherwise we'll get disconnected.
            self.send("PONG %s" % args[0])
        if (cmd == "PRIVMSG"):
            # Either a channel message or a private message; check and display.
            message = ' '.join(args[1:])
            nick = prefix[:prefix.find('!')]
            if (args[1].startswith(chr(1))):
                ctcp = message.translate(None, chr(1)).split()
                ctcp_cmd = ctcp[0]
                ctcp_msg = ' '.join(ctcp[1:])
                self.handle_ctcp(ctcp_cmd, ctcp_msg)
            elif (args[0] == self.channel):
                try:
                    role, content = message.split(" <-> ")
                    if role == "attacker":
                        dc = self.monosub.decypher(content)
                        message = "%s <-> %s" % (role, dc)
                    elif role == "defender":
                        dc = self.vigenere.decypher(content)
                        message = "%s <-> %s" % (role, dc)
                    if self.cyphered_logging and role != self.role:
                        print(content, file=self.message_fd)
                        self.message_fd.flush()
                except:
                    pass
                self.ui.add_nick_message(nick, message)
            else:
                self.ui.add_private_message(nick, message)
        if (cmd == "JOIN"):
            nick = prefix[:prefix.find('!')]
            if (not self.joined):
                # We weren't joined, so join message must be us joining.
                self.joined = True
                self.channel = args[0]
                self.ui.update_status()
                self.print_status("joined channel %s " % self.channel)
            elif (nick != self.nick):
                # A user has joined the channel. Update nick list.
                self.add_nick(prefix[:prefix.find('!')])
                self.print_status("%s joined the channel" % nick)
        if (cmd == "PART" and args[0] == self.channel):
            # A user has left the channel. Update nick list.
            nick = prefix[:prefix.find('!')]
            self.del_nick(nick)
            self.print_status("%s left the channel" % nick)
        if (cmd == "353"):
            # Receiving a list of users in the channel (aka RPL_NAMEREPLY).
            # Note that the user list may span multiple 353 messages.
            nicklist = ' '.join(args[3:]).split()
            self.set_nicklist(nicklist)
        if (cmd == "376"):
            # Finished receiving the message of the day (MOTD).
            self.print_status("MOTD received, ready for action")
            self.ui.update_status()
        if (cmd == "NICK"):
            old = prefix[:prefix.find('!')]
            new = args[0]
            if (old == self.nick):
                # server acknowledges we changed our own nick
                self.nick = new
            self.replace_nick(old, new)
            self.ui.update_status()
