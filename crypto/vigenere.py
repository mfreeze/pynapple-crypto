import unidecode
import re

class Vigenere(object):

    """Simple Vigenere cyphering and decyphering"""

    def __init__(self, key):
        """Initialize a vigenere key. Only alphabetic characters are
        considered, numeric and special characters are stripped out from
        the key. Alphabetic characters are lowered to compute the key.

        :key: the string that is used as a vigenere mask
        """

        # The list of non alphabetic lower caracters
        low_alpha_regex = re.compile('[^a-z]')

        self._keystream = [ ord(c) - 96 for c in low_alpha_regex.sub('',
                                                                     unidecode.unidecode(key).lower()) ]
        self._keylen = len(self._keystream)
        
    def cypher(self, message):
        norm_str = unidecode.unidecode(message).lower()
        cur_index = 0
        result = ""
        for i in [ ord(c) - 97 for c in norm_str ]:
            if 0 <= i < 26:
                result += chr(((i + self._keystream[cur_index]) % 26) + 97)
                cur_index = (cur_index + 1) % self._keylen
            else:
                result += chr(i + 97)
        return result

    def decypher(self, message):
        norm_str = unidecode.unidecode(message).lower()
        cur_index = 0
        result = ""
        for i in [ ord(c) - 97 for c in norm_str ]:
            if 0 <= i < 26:
                result += chr(((i - self._keystream[cur_index]) % 26) + 97)
                cur_index = (cur_index + 1) % self._keylen
            else:
                result += chr(i + 97)
        return result

