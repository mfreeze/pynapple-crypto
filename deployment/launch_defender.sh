#!/bin/bash

cd /mnt/c/Users/Student/Desktop/pynapple

answer=""
attack_key=""
defend_key=""
pseudo=""

echo -n "Entrez votre pseudo: "
read -r pseudo

while [ "$answer" != "o" ] && [ "$answer" != "n" ]; do
  echo -n "Avez-vous trouvé la clé des attaquants (o/n)? "
  read -r answer
done


if [ "$answer" = "o" ]; then
  echo -n "Entrez la clé des attaquants: "
  read -r attack_key
fi

echo -n "Entrez la clé des défenseurs: "
read -r defend_key


if [ -n "$attack_key" ]; then
  DISPLAY=localhost:0.0 python3 -m pynapple.pynapple -n "$pseudo" -r "defender" -l ../log_file.txt  -L ../messages.txt -d "$defend_key" -a "$attack_key"
else
  DISPLAY=localhost:0.0 python3 -m pynapple.pynapple -n "$pseudo" -r "defender" -l ../log_file.txt  -L ../messages.txt -d "$defend_key"
fi
