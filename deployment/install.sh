#!/bin/bash

sudo bash -c "apt update && apt dist-upgrade --assume-yes && apt install --assume-yes python3 python3-tk python3-unidecode git"

cd /mnt/c/Users/Student/Desktop || exit 1

git clone https://gitlab.com/mfreeze/pynapple-crypto.git pynapple

cd pynapple || exit 1
# DISPLAY=localhost:0.0 python3 -m pynapple.pynapple
