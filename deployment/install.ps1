# Install wsl
wsl.exe --install -d debian

# Go on the Desktop
Set-Location -Path C:\Users\Student\Desktop

$X_Path = "C:\Program Files\VcXsrv\vcxsrv.exe"

If (!(Test-Path $X_Path -PathType Leaf )) {
  # Download vcxsrv
  Invoke-WebRequest -UserAgent "Wget" -Uri https://sourceforge.net/projects/vcxsrv/files/latest/download -OutFile vcxsrv.exe

  # Install X server
  Start-Process ./vcxsrv.exe -Wait
}

If (!(Get-Process -Name 'vcxsrv')) {
  # First launch
  Start-Process "C:\Program Files\VcXsrv\xlaunch.exe"
}

# Download shell script
Invoke-WebRequest -Uri "https://gitlab.com/mfreeze/pynapple-crypto/-/raw/master/deployment/install.sh?ref_type=heads"  -UserAgent "Wget" -OutFile "install.sh"

# Launch wsl script
wsl.exe -e ./install.sh
